﻿using FA.Net06.Financial.Data.Entities;
using FA.Net06.Financial.DTOs.Response;
using FA.Net06.Financial.DTOs.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Services.UserService
{
    public interface IUserService
    {
        Task<IEnumerable<Claim>> GetClaims(Guid id);
        Task<Response<object>> SendResetPasswordEmail(string email);
        Task<Response<object>> ResetPassword(ResetPasswordDTO resetPassword);
        Task<Response<AddUserDTO>> AddUser(AddUserDTO newUser);
        Task<Response<UserDetailsDTO>> DetailsUser(Guid userId);
        Task<Response<object>> ConfirmResetPassword(string token);
        Task<Response<UpdateUserDTO>> UpdateUser(UpdateUserDTO  updateUser);
        Task<Response<User>> Delete(Guid id);
        Task<Response<Paginator<UserDTO>>> GetPagedUsers(int pageIndex, int pageSize, string? roleName, string userName);
    }
}
