﻿using AutoMapper;
using FA.Net06.Financial.Data.Entities;
using FA.Net06.Financial.DTOs.Response;
using FA.Net06.Financial.DTOs.User;
using FA.Net06.Financial.Repository.Infrastructure;
using FA.Net06.Financial.Services.EmailService;
using FA.Net06.Financial.Services.Validators;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Net.Mail;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using FA.Net06.Financial.Services.Validators;
using FluentValidation.AspNetCore;
using System.Web;
using System.Security.Cryptography;
using System.Linq.Expressions;

namespace FA.Net06.Financial.Services.UserService
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly IEmailService _emailService;

        public UserService(IUnitOfWork unitOfWork, IMapper mapper, UserManager<User> userManager, IEmailService emailService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _userManager = userManager;
            _emailService = emailService;
        }

        public async Task<IEnumerable<Claim>> GetClaims(Guid id)
        {
            var user = await _unitOfWork.UserRepository.GetDetails(id);
            var claims = new List<Claim>
            {
                new Claim("Id", user.Id.ToString()),
                new Claim("Name", user.FullName),
                new Claim("Department", user.Department.Name)
            };
            foreach (var role in user.Roles)
                claims.Add(new Claim("Role", role.Name));

            return claims;
        }

        private string GeneratePasswordResetToken()
        {
            var randomNumber = new byte[64];
            using (var randomNumberGenerator = RandomNumberGenerator.Create())
            {
                randomNumberGenerator.GetBytes(randomNumber);
                return Convert.ToHexString(randomNumber);
            }
        }

        public async Task<Response<object>> SendResetPasswordEmail(string email)
        {
            var response = new Response<object> { Succeeded = true };
            var user = await _unitOfWork.UserRepository.FindByValue(user => user.Email == email);
            if (user == null)
            {
                response.Succeeded = false;
                response.Errors.AddModelError("Email", "Email does not exist");
                return response;
            }

            try
            {
                var resetPasswordToken = GeneratePasswordResetToken();
                user.ResetPasswordToken = resetPasswordToken;
                user.ResetPasswordExpire = DateTime.Now.AddDays(1);
                _unitOfWork.UserRepository.Update(user);
                await _unitOfWork.SaveChangesAsync();
                string title = $"Yêu cầu khôi phục mật khẩu từ tài khoản {user.UserName}";
                string content = $"Ai đó đã yêu cầu khôi phục mật khẩu của tài khoản {user.UserName}, nếu đó là bạn, click vào link sau để xác nhận:\n<a href=\"http://103.130.213.161:7500/user/resetPassword/{resetPasswordToken}\">Confirm reset password</a>";
                _emailService.Send(title, content, email);
                return response;
            }
            catch
            {
                response.Succeeded = false;
                response.Errors.AddModelError("Email", "Cannot send confirm reset password email");
                return response;
            }
        }

        public async Task<Response<object>> ConfirmResetPassword(string token)
        {
            var response = new Response<object> { Succeeded = true };
            var user = await _unitOfWork.UserRepository.FindByValue(user => user.ResetPasswordToken == token);
            if (user == null || user.ResetPasswordExpire < DateTime.Now)
            {
                response.Succeeded = false;
                response.Errors.AddModelError("token", "This link has expired. Please go back to Homepage and try again");
            }
            return response;
        }

        public async Task<Response<object>> ResetPassword(ResetPasswordDTO resetPassword)
        {
            var response = new Response<object> { Succeeded = true };
            var validator = new ResetPasswordDTOValidator();
            var result = validator.Validate(resetPassword);
            if (!result.IsValid)
            {
                response.Succeeded = false;
                result.AddToModelState(response.Errors);
                return response;
            }

            try
            {
                var user = await _unitOfWork.UserRepository.FindByValue(user => user.ResetPasswordToken == resetPassword.Token);
                var hasher = new PasswordHasher<User>();
                user.PasswordHash = hasher.HashPassword(user, resetPassword.NewPassword);
                user.ResetPasswordToken = null;
                _unitOfWork.UserRepository.Update(user);
                await _unitOfWork.SaveChangesAsync();
                return response;
            }
            catch
            {
                response.Succeeded = false;
                response.Errors.AddModelError("token", "Reset password token is not valid");
                return response;
            }
        }

        private static readonly string[] VietNamChar = new string[]
        {
            "aAeEoOuUiIdDyY",
            "áàạảãâấầậẩẫăắằặẳẵ",
            "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",
            "éèẹẻẽêếềệểễ",
            "ÉÈẸẺẼÊẾỀỆỂỄ",
            "óòọỏõôốồộổỗơớờợởỡ",
            "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",
            "úùụủũưứừựửữ",
            "ÚÙỤỦŨƯỨỪỰỬỮ",
            "íìịỉĩ",
            "ÍÌỊỈĨ",
            "đ",
            "Đ",
            "ýỳỵỷỹ",
            "ÝỲỴỶỸ"
        };

        public static string LocDau(string str)
        {
            //Thay thế và lọc dấu từng char      
            for (int i = 1; i < VietNamChar.Length; i++)
            {
                for (int j = 0; j < VietNamChar[i].Length; j++)
                    str = str.Replace(VietNamChar[i][j], VietNamChar[0][i - 1]);
            }
            return str;
        }

        private async Task<string> CreateUserName(string name)
        {
            var users = await _unitOfWork.UserRepository.GetAll();
            var nameKoDau = LocDau(name);
            int count = 1;
            var names = nameKoDau.Split(' ');
            var lastName = char.ToUpper(names[names.Length - 1][0]) + names[names.Length - 1].Remove(0, 1).ToLower();
            var shortFirstName = string.Empty;
            for (int i = 0; i < names.Length - 1; i++)
            {
                shortFirstName += char.ToUpper(names[i][0]);
            }
            foreach (var user in users)
            {
                if (user.FullName.Contains(name))
                {
                    count++;
                }
            }
            return $"{lastName}{shortFirstName}"+count.ToString();
        }

        private async Task<string> NormFullName(string name)
        {
            name = name.Trim();
            name = Regex.Replace(name, @"\s+", " ");
            return name;
        }
        /// <summary>
        /// Gửi UserName vs PassWord cho người dùng qua email
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private async Task<bool> SendEmailCreateUser(User user)
        {
            if (user == null)
                return false;

            string title = $"Tạo tài khoản thành công:";
            string content = $"UserName là: {user.UserName}\n Mật khẩu mặc định là : Matkhau1234! ";
            _emailService.Send(title, content, user.Email);
            return true;
        }

        /// <summary>
        /// Add New User
        /// => 
        /// </summary>
        /// <param name="newUser"></param>
        /// <returns></returns>
        public async Task<Response<AddUserDTO>> AddUser(AddUserDTO newUser)
        {
            var serviceResponse = new Response<AddUserDTO>()
            {
                Succeeded = true
            };
            newUser.Fullname = await NormFullName(newUser.Fullname);
            // validate 
            var validator = new AddUserDTOValidator();
            var result = validator.Validate(newUser);
            if (!result.IsValid)
            {
                serviceResponse.Succeeded = false;
                serviceResponse.Data = null;
                result.AddToModelState(serviceResponse.Errors);
                return serviceResponse;
            }

            // check email exists
            var userByEmail = await _unitOfWork.UserRepository.FindByValue(u => u.Email == newUser.Email);
            if (userByEmail != null)
            {
                serviceResponse.Succeeded = false;
                serviceResponse.Data = null;
                serviceResponse.Errors.AddModelError("Email", "Email Had Used");
                return serviceResponse;
            }

            // check phone number exists
            var userByPhone = await _unitOfWork.UserRepository.FindByValue(u => u.PhoneNumber == newUser.Phonenumber);
            if (userByPhone != null)
            {
                serviceResponse.Succeeded = false;
                serviceResponse.Data = null;
                serviceResponse.Errors.AddModelError("Phone Number", "Phone Number Had Used");
                return serviceResponse;
            }

            // Check Role 
            var role = await _unitOfWork.RoleRepository.Find(newUser.RoleId);
            if (role == null)
            {
                serviceResponse.Succeeded = false;
                serviceResponse.Data = null;
                serviceResponse.Errors.AddModelError("Role", "Role Does not Exist");
                return serviceResponse;
            }

            // Check Position 
            var position = await _unitOfWork.UserRepository.GetPosition(newUser.PositionId);
            if (position == null)
            {
                serviceResponse.Succeeded = false;
                serviceResponse.Data = null;
                serviceResponse.Errors.AddModelError("Position", "Position Does not Exist");
                return serviceResponse;
            }

            // Check Department 
            var department = await _unitOfWork.UserRepository.GetDepartment(newUser.DepartmentId);
            if (department == null)
            {
                serviceResponse.Succeeded = false;
                serviceResponse.Data = null;
                serviceResponse.Errors.AddModelError("Department", "Department Does not Exist");
                return serviceResponse;
            }

            try
            {
                // Mapping to User 
                var userAdd = _mapper.Map<User>(newUser);
                userAdd.FullName = await NormFullName(newUser.Fullname);
                userAdd.Email = userAdd.Email.ToLower();

                // Add Role to User
                userAdd.Roles = new Role[] { role };

                // BRL-28-01
                userAdd.UserName = await CreateUserName(userAdd.FullName);

                // Normalized User Name and Email
                userAdd.NormalizedUserName = userAdd.UserName.ToUpper();
                userAdd.NormalizedEmail = userAdd.Email.ToUpper();

                // SecurityStamp
                userAdd.SecurityStamp = Guid.NewGuid().ToString();

                // BRL-28-04
                userAdd.Status = "Active";

                // BRL-28-03
                var hasher = new PasswordHasher<User>();
                userAdd.PasswordHash = hasher.HashPassword(userAdd, "Matkhau1234!");

                // Add
                await _unitOfWork.UserRepository.Create(userAdd);
                await _unitOfWork.SaveChangesAsync();
                await SendEmailCreateUser(userAdd);
            }
            catch
            {
                serviceResponse.Succeeded = false;
                serviceResponse.Data = null;
                serviceResponse.Errors.AddModelError("Other", "Exception Occurs");
            }
            return serviceResponse;
        }
        /// <summary>
        /// Details User
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Response<UserDetailsDTO>> DetailsUser(Guid userId)
        {
            var user = await _unitOfWork.UserRepository.GetDetails(userId);
            var serviceResponse = new Response<UserDetailsDTO>()
            {
                Succeeded = true
            };
            try
            {
                var userDetail = _mapper.Map<UserDetailsDTO>(user);
                serviceResponse.Data = userDetail;
                serviceResponse.Succeeded = true;
            }
            catch
            {
                serviceResponse.Succeeded = false;
                serviceResponse.Errors.AddModelError("UserId", "Cannot Find User");
            }
            return serviceResponse;
        }
        /// <summary>
        /// Update User
        /// </summary>
        /// <param name="updateUser"></param>
        /// <returns></returns>
        public async Task<Response<UpdateUserDTO>> UpdateUser(UpdateUserDTO updateUser)
        {
            var serviceResponse = new Response<UpdateUserDTO>() 
            {
                Succeeded =true
            };
            
            updateUser.FullName = await NormFullName(updateUser.FullName);
            // validate 
            var validator = new UpdateUserDTOValidator();
            var result = validator.Validate(updateUser);
            if (!result.IsValid)
            {
                serviceResponse.Succeeded = false;
                serviceResponse.Data = null;
                result.AddToModelState(serviceResponse.Errors);
                return serviceResponse;
            }
            // check User Exist
            var addUser = await _unitOfWork.UserRepository.Find(updateUser.Id);
            if (addUser == null)
            {
                serviceResponse.Succeeded = false;
                serviceResponse.Data = null;
                serviceResponse.Errors.AddModelError("User", "User Not Exist");
                return serviceResponse;
            }
            // check email exists
            var userByEmail = await _unitOfWork.UserRepository.FindByValue(u => u.Email == updateUser.Email);
            if (userByEmail != null)
            {
                serviceResponse.Succeeded = false;
                serviceResponse.Data = null;
                serviceResponse.Errors.AddModelError("Email", "Email Had Used");
                return serviceResponse;
            }

            // check phone number exists
            var userByPhone = await _unitOfWork.UserRepository.FindByValue(u => u.PhoneNumber == updateUser.PhoneNumber);
            if (userByPhone != null)
            {
                serviceResponse.Succeeded = false;
                serviceResponse.Data = null;
                serviceResponse.Errors.AddModelError("Phone Number", "Phone Number Had Used");
                return serviceResponse;
            }

            // Check Role 
            var role = await _unitOfWork.RoleRepository.Find(updateUser.RoleId);
            if (role == null)
            {
                serviceResponse.Succeeded = false;
                serviceResponse.Data = null;
                serviceResponse.Errors.AddModelError("Role", "Role Does not Exist");
                return serviceResponse;
            }

            //// Check Position 
            //var position = await _unitOfWork.UserRepository.Find(updateUser.PositionId);
            //if (position == null)
            //{
            //    serviceResponse.Succeeded = false;
            //    serviceResponse.Data = null;
            //    serviceResponse.Errors.AddModelError("Position", "Position Does not Exist");
            //    return serviceResponse;
            //}

            //// Check Department 
            //var department = await _unitOfWork.UserRepository.Find(updateUser.DepartmentId);
            //if (department == null)
            //{
            //    serviceResponse.Succeeded = false;
            //    serviceResponse.Data = null;
            //    serviceResponse.Errors.AddModelError("Department", "Department Does not Exist");
            //    return serviceResponse;
            //}

            try
            {
                // Mapping to User 
                var userAdd = new User()
                {
                    FullName = updateUser.FullName,
                    Email = updateUser.Email,
                    PhoneNumber = updateUser.PhoneNumber,
                    DOB = updateUser.DOB,
                    Address = updateUser.Address,
                    DepartmentId = updateUser.DepartmentId,
                    PositionId = updateUser.PositionId,
                    Note = updateUser.Note,
                };
                userAdd.FullName = await NormFullName(updateUser.FullName);
                userAdd.Email = userAdd.Email.ToLower();

                // Add Role to User
                userAdd.Roles = new Role[] { role };


                // BRL-28-01
                userAdd.UserName = await CreateUserName(userAdd.FullName);

                // Normalized User Name and Email
                userAdd.NormalizedUserName = userAdd.UserName.ToUpper();
                userAdd.NormalizedEmail = userAdd.Email.ToUpper();

                // SecurityStamp
                userAdd.SecurityStamp = Guid.NewGuid().ToString();

                // Update
                _unitOfWork.UserRepository.Update(userAdd);
                await _unitOfWork.SaveChangesAsync();
            }
            catch
            {
                serviceResponse.Succeeded = false;
                serviceResponse.Data = null;
                serviceResponse.Errors.AddModelError("Other", "Exception Occurs");
            }
            return serviceResponse;
        }

        public async Task<Response<User>> Delete(Guid id)
        {
            var serviceResponse = new Response<User>
            {
                Succeeded = true
            };

            try
            {
                var user = await _unitOfWork.UserRepository.Find(id);
                await _unitOfWork.UserRepository.Delete(id);
                _unitOfWork.SaveChanges();
                return serviceResponse;
            }
            catch
            {
                serviceResponse.Succeeded = false;
                serviceResponse.Errors.AddModelError("Delete User", "Cannot delete user");
                return serviceResponse;
            }
        }

        public async Task<Response<Paginator<UserDTO>>> GetPagedUsers(int pageIndex, int pageSize, string? roleName, string userName)
        {
            var serviceResponse = new Response<Paginator<UserDTO>>
            {
                Succeeded = true
            };
            int total;
            pageSize = 10;
            try
            {
                var predicates = new List<Expression<Func<User, bool>>>();
                if (!string.IsNullOrEmpty(roleName))
                {
                    var listUser = await _unitOfWork.UserRepository.GetRole(roleName);
                    foreach (var item in listUser)
                    {
                        predicates.Add(user => user.Id == item.Id);
                    }

                }

                if (!string.IsNullOrEmpty(userName))
                    predicates.Add(user => user.UserName.Contains(userName));

                var users = _unitOfWork.UserRepository.GetPagedList(pageIndex, pageSize, predicates, out total).Select(user => _mapper.Map<UserDTO>(user));
                serviceResponse.Data = new Paginator<UserDTO>
                {
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    Total = total,
                    Items = users
                };
                return serviceResponse;
            }
            catch
            {
                serviceResponse.Succeeded = false;
                serviceResponse.Errors.AddModelError("GetPagedUsers", "Cannot get paged users");
                return serviceResponse;
            }
        }
    }
}
