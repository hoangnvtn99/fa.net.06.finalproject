﻿using AutoMapper;
using FA.Net06.Financial.Data.Entities;
using FA.Net06.Financial.DTOs.FinancialPlan;
using FA.Net06.Financial.DTOs.Term;
using FA.Net06.Financial.DTOs.User;

namespace FA.Net06.Financial.Services.Mapper
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            CreateMap<UserDTO, User>().ReverseMap();
            CreateMap<User, AddUserDTO>().ReverseMap();
            CreateMap<User, UpdateUserDTO>().ReverseMap();

            CreateMap<UserDetailsDTO, User>().ReverseMap()
               .ForMember(userDetailsDTO => userDetailsDTO.DepartmentName,
                           user => user.MapFrom(src => src.Department.Name))
               .ForMember(userDetailsDTO => userDetailsDTO.PositionName,
                           user => user.MapFrom(src => src.Position.Name))
               .ForMember(userDetailsDTO => userDetailsDTO.RoleName,
                           user => user.MapFrom(src => src.Roles.Select(role => role.Name).First()));
            CreateMap<NewTermDTO, Term>().ReverseMap();
            CreateMap<Term, TermDTO>().ReverseMap();
            CreateMap<Term, TermDetailsDTO>().ReverseMap();
            CreateMap<Term, EditTermDTO>().ReverseMap();
            CreateMap<FinancialPlan, FinancialPlanDTO>()
                .ForMember(target => target.Term, m => m.MapFrom(src => src.Term.Name))
                .ForMember(target => target.Department, m => m.MapFrom(src => src.Department.Name))
                .ForMember(target => target.Uploader, m => m.MapFrom(src => src.User.UserName));
        }
    }
}