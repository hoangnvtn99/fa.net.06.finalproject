﻿using FA.Net06.Financial.DTOs.User;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Services.Validators
{
    public class AddUserDTOValidator: AbstractValidator<AddUserDTO>
    {
        public AddUserDTOValidator()
        {
            // Check length of FullName 
            //RuleFor(addUser => addUser.Fullname).Custom((fullName, context) =>
            //{
            //    if(fullName.Split(' ').Length < 3)
            //    {
            //        context.AddFailure("Full Name must have minimum three words");
            //    }
            //});
            RuleFor(addUser => addUser.Fullname).Must(fullName => fullName.Split(' ').Length >= 3);

            // BRL-28-06
            RuleFor(addUser => addUser.Email).NotNull().NotEmpty().EmailAddress();

            // BRL-28-05
            RuleFor(addUser => addUser.DOB).NotNull().NotEmpty().LessThan(DateTime.Now).WithMessage("Date of birth must be the past date");

            // Check Phone Number
            RuleFor(addUser => addUser.Phonenumber).NotNull().NotEmpty().Length(10).Must(phoneNumber => phoneNumber.All(number => char.IsDigit(number)));
        }
    }
}
