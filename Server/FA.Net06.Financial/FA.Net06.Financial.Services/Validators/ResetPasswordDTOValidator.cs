﻿using FA.Net06.Financial.DTOs.Term;
using FA.Net06.Financial.DTOs.User;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Services.Validators
{
    public class ResetPasswordDTOValidator : AbstractValidator<ResetPasswordDTO>
    {
        public ResetPasswordDTOValidator()
        {
            var regexContainsDigit = new Regex(@"\d+");
            var regexContainsLetter = new Regex(@"[A-z]+");
            RuleFor(resetPassword => resetPassword.NewPassword)
                .Must(newPassword => regexContainsDigit.IsMatch(newPassword) && regexContainsLetter.IsMatch(newPassword) && newPassword.Length >= 7)
                .WithMessage("Password needs to have at least one letter, one number and seven characters");
            RuleFor(resetPassword => new { resetPassword.NewPassword, resetPassword.ConfirmNewPassword })
                .Must(resetPassword => resetPassword.NewPassword == resetPassword.ConfirmNewPassword)
                .WithMessage("Password and Confirm password don't match");
        }
    }
}
