﻿using FA.Net06.Financial.DTOs.User;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Services.Validators
{
    public class UpdateUserDTOValidator: AbstractValidator<UpdateUserDTO>
    {
        public UpdateUserDTOValidator()
        {
            
            RuleFor(addUser => addUser.FullName).Must(fullName => fullName.Split(' ').Length >= 3);

            // BRL-28-06
            RuleFor(addUser => addUser.Email).NotNull().NotEmpty().EmailAddress();

            // BRL-28-05
            RuleFor(addUser => addUser.DOB).NotNull().NotEmpty().LessThan(DateTime.Now).WithMessage("Date of birth must be the past date");

            // Check Phone Number
            RuleFor(addUser => addUser.PhoneNumber).NotNull().NotEmpty().Length(10).Must(phoneNumber => phoneNumber.All(number => char.IsDigit(number)));
        }
    }
}
