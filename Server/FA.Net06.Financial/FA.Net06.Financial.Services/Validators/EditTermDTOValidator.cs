﻿using FA.Net06.Financial.DTOs.Term;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Services.Validators
{
    public class EditTermDTOValidator : AbstractValidator<EditTermDTO>
    {
        public EditTermDTOValidator()
        {
            var durations = new List<string> { "", "Monthly", "", "", "Quarterly", "", "Half-year" };
            RuleFor(term => term.Name).NotEmpty();
            RuleFor(term => term.Duration).Matches(@"Monthly|Quarterly|Half-year").WithMessage("Duration of term must be Monthly, Quarterly or Half-year");
            RuleFor(term => term.StartDate).GreaterThan(DateTime.Now).WithMessage("Start date must be later than now");
            RuleFor(term => new { term.StartDate, term.EndDate, term.Duration })
                .Must(term => term.EndDate == term.StartDate.AddMonths(durations.IndexOf(term.Duration)))
                .WithMessage("End date must be equal sum of start date and duration");
            RuleFor(term => new { term.PlanDueDate, term.ReportDueDate, term.EndDate })
                .Must(term => term.PlanDueDate > term.EndDate && term.ReportDueDate > term.EndDate)
                .WithMessage("Plan due date and report due date must be later than end date of the term");
        }
    }
}
