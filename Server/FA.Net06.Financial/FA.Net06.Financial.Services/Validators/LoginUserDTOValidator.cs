﻿using FA.Net06.Financial.DTOs.User;
using FluentValidation;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Services.Validators
{
    public class LoginUserDTOValidator : AbstractValidator<LoginUserDTO>
    {
        public LoginUserDTOValidator()
        {
            var regex = new Regex(@"\d+");
            RuleFor(loginUser => loginUser.Password).NotNull().NotEmpty().MinimumLength(7).Must(password => password.Length >= 7 && regex.IsMatch(password));
            RuleFor(loginUser => loginUser.Email).NotNull().NotEmpty().EmailAddress();
        }
    }
}
