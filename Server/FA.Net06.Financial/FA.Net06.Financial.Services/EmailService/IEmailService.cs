﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Services.EmailService
{
    public interface IEmailService
    {
        void Send(string title, string content, string to);
    }
}
