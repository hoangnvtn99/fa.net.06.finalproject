﻿using AutoMapper;
using FA.Net06.Financial.Data.Entities;
using FA.Net06.Financial.DTOs.FinancialPlan;
using FA.Net06.Financial.DTOs.Response;
using FA.Net06.Financial.DTOs.Term;
using FA.Net06.Financial.Repository.Infrastructure;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Services.FinancialPlanService
{
    public class FinancialPlanService : IFinancialPlanService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IConfiguration _config;

        public FinancialPlanService(IUnitOfWork unitOfWork, IMapper mapper, IConfiguration config)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _config = config;
        }

        public Response<Paginator<FinancialPlanDTO>> GetPagedPlans(int pageIndex, int pageSize, string name, string department, string term, string status, string departmentOfViewer, string roleOfViewer)
        {
            var response = new Response<Paginator<FinancialPlanDTO>> { Succeeded = true };
            var predicates = new List<Expression<Func<FinancialPlan, bool>>>();
            var statusListForOtherDepartments = new List<string> { "Waiting for Approval", "Approved" };

            if (roleOfViewer == "Staff")
            {
                if (!string.IsNullOrEmpty(department))
                    predicates.Add(plan => plan.Department.Name == department);
                else
                    predicates.Add(plan => plan.Department.Name == departmentOfViewer);

                if (!string.IsNullOrEmpty(status))
                    predicates.Add(plan => plan.Status == status);
            }

            if (roleOfViewer == "Accountant")
            {
                if (string.IsNullOrEmpty(department) && string.IsNullOrEmpty(status))
                {
                    predicates.Add(plan =>
                        plan.Department.Name == departmentOfViewer ||
                        statusListForOtherDepartments.Contains(plan.Status)
                    );
                }

                if (!string.IsNullOrEmpty(department) && string.IsNullOrEmpty(status))
                {
                    if (department == departmentOfViewer)
                        predicates.Add(plan => plan.Department.Name == departmentOfViewer);
                    else
                        predicates.Add(plan => plan.Department.Name == department && statusListForOtherDepartments.Contains(plan.Status));
                }

                if (string.IsNullOrEmpty(department) && !string.IsNullOrEmpty(status))
                {
                    if (statusListForOtherDepartments.Contains(status))
                        predicates.Add(plan => plan.Status == status);
                    else
                        predicates.Add(plan => plan.Status == status && plan.Department.Name == departmentOfViewer);
                }

                if (!string.IsNullOrEmpty(department) && !string.IsNullOrEmpty(status))
                {
                    if (department == departmentOfViewer)
                        predicates.Add(plan => plan.Department.Name == departmentOfViewer);
                    else
                        predicates.Add(plan => plan.Department.Name == department && statusListForOtherDepartments.Contains(status));
                }
            }

            if (!string.IsNullOrEmpty(name))
                predicates.Add(plan => plan.Name == name);
            if (!string.IsNullOrEmpty(term))
                predicates.Add(plan => plan.Term.Name == term);

            try
            {
                int total;
                var plans = _unitOfWork.FinancialPlanRepository.GetPagedList(pageIndex, pageSize, predicates, out total)
                                                               .Select(plan => _mapper.Map<FinancialPlanDTO>(plan));
                response.Data = new Paginator<FinancialPlanDTO>
                {
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    Total = total,
                    Items = plans
                };
                return response;
            }
            catch
            {
                response.Succeeded = false;
                response.Errors.AddModelError("Get Plans", "No items match your credentials, please try again");
                return response;
            }
        }

        public Response<ImportPlanDTO> Import()
        {
            var response = new Response<ImportPlanDTO> { Succeeded = true };
            try
            {
                var conditions = new List<Expression<Func<Term, bool>>> { term => term.Status == "In-progress" };
                var terms = _unitOfWork.TermRepository.Find(conditions);
                var fileTemplateUrl = _config["FileTemplateUrl:Plan"];
                response.Data = new ImportPlanDTO
                {
                    FileTemplateUrl = fileTemplateUrl,
                    Terms = terms.Select(term => _mapper.Map<TermDTO>(term))
                };
                return response;
            }
            catch
            {
                response.Succeeded = false;
                return response;
            }
        }
    }
}