﻿using FA.Net06.Financial.DTOs.FinancialPlan;
using FA.Net06.Financial.DTOs.Response;
using FA.Net06.Financial.DTOs.Term;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Services.FinancialPlanService
{
    public interface IFinancialPlanService
    {
        Response<Paginator<FinancialPlanDTO>> GetPagedPlans(int pageIndex, int pageSize, string name, string department, string term, string status, string departmentOfViewer, string roleOfViewer);
        Response<ImportPlanDTO> Import();
    }
}
