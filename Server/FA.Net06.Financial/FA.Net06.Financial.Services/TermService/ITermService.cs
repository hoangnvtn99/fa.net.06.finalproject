﻿using FA.Net06.Financial.DTOs.Response;
using FA.Net06.Financial.DTOs.Term;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Services.TermService
{
    public interface ITermService
    {
        Task<Response<NewTermDTO>> Add(NewTermDTO newTerm);
        Response<Paginator<TermDTO>> GetPagedTerms(int pageIndex, int pageSize, string status, string termName);
        Task<Response<TermDetailsDTO>> GetDetails(Guid id);
        Task<Response<EditTermDTO>> Edit(EditTermDTO editTerm);
        Task<Response<object>> Delete(Guid id);
        Task<Response<object>> Start(Guid id);
    }
}
