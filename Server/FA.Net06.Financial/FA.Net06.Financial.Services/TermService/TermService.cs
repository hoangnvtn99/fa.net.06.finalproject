﻿using AutoMapper;
using FA.Net06.Financial.Data.Entities;
using FA.Net06.Financial.DTOs.Response;
using FA.Net06.Financial.DTOs.Term;
using FA.Net06.Financial.Repository.Infrastructure;
using FA.Net06.Financial.Services.Validators;
using FluentValidation.AspNetCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Services.TermService
{
    public class TermService : ITermService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TermService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Response<NewTermDTO>> Add(NewTermDTO newTerm)
        {
            var response = new Response<NewTermDTO>
            {
                Succeeded = true
            };

            var validator = new NewTermDTOValidator();
            var result = validator.Validate(newTerm);
            if (!result.IsValid)
            {
                response.Succeeded = false;
                result.AddToModelState(response.Errors);
                return response;
            }

            try
            {
                var term = _mapper.Map<Term>(newTerm);
                term.Id = Guid.NewGuid();
                await _unitOfWork.TermRepository.Create(term);
                await _unitOfWork.SaveChangesAsync();
            }
            catch
            {
                response.Errors.AddModelError("Create", "Cannot create new term");
                response.Succeeded = false;
            }
            return response;
        }

        public Response<Paginator<TermDTO>> GetPagedTerms(int pageIndex, int pageSize, string status, string termName)
        {
            var response = new Response<Paginator<TermDTO>>
            {
                Succeeded = true
            };
            int total;
            pageSize = 10;
            try
            {
                var predicates = new List<Expression<Func<Term, bool>>>();
                if (!string.IsNullOrEmpty(status))
                    predicates.Add(term => term.Status == status);
                if (!string.IsNullOrEmpty(termName))
                    predicates.Add(term => term.Name == termName);

                var terms = _unitOfWork.TermRepository.GetPagedList(pageIndex, pageSize, predicates, out total).Select(term => _mapper.Map<TermDTO>(term));
                response.Data = new Paginator<TermDTO>
                {
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    Total = total,
                    Items = terms
                };
                return response;
            }
            catch
            {
                response.Succeeded = false;
                response.Errors.AddModelError("GetPagedTerms", "Cannot get paged terms");
                return response;
            }
        }

        public async Task<Response<TermDetailsDTO>> GetDetails(Guid id)
        {
            var response = new Response<TermDetailsDTO>
            {
                Succeeded = true
            };

            try
            {
                var term = await _unitOfWork.TermRepository.Find(id);
                response.Data = _mapper.Map<TermDetailsDTO>(term);
                return response;
            }
            catch
            {
                response.Succeeded = false;
                response.Errors.AddModelError("GetTerm", "Not found");
                return response;
            }
        }

        public async Task<Response<EditTermDTO>> Edit(EditTermDTO editTerm)
        {
            var response = new Response<EditTermDTO>
            {
                Succeeded = true
            };

            var validator = new EditTermDTOValidator();
            var result = validator.Validate(editTerm);
            if (!result.IsValid)
            {
                response.Succeeded = false;
                result.AddToModelState(response.Errors);
                return response;
            }

            try
            {
                var term = _mapper.Map<Term>(editTerm);
                _unitOfWork.TermRepository.Update(term);
                await _unitOfWork.SaveChangesAsync();
                return response;
            }
            catch
            {
                response.Succeeded = false;
                response.Errors.AddModelError("Update Term", "Cannot update term");
                return response;
            }
        }

        public async Task<Response<object>> Delete(Guid id)
        {
            var response = new Response<object>
            {
                Succeeded = true
            };

            try
            {
                var term = await _unitOfWork.TermRepository.Find(id);

                if (term.Status != "New")
                {
                    response.Succeeded = false;
                    response.Errors.AddModelError("Delete Term", "Only new Term can be deleted");
                    return response;
                }

                await _unitOfWork.TermRepository.Delete(id);
                await _unitOfWork.SaveChangesAsync();
                return response;
            }
            catch
            {
                response.Succeeded = false;
                response.Errors.AddModelError("Delete Term", "Cannot delete term");
                return response;
            }
        }

        public async Task<Response<object>> Start(Guid id)
        {
            var response = new Response<object> { Succeeded = true };
            try
            {
                var term = await _unitOfWork.TermRepository.Find(id);
                if (term.Status != "New")
                {
                    response.Succeeded = false;
                    response.Errors.AddModelError("Start term", "Only new terms can start");
                    return response;
                }
                term.Status = "In-progress";
                _unitOfWork.TermRepository.Update(term);
                await _unitOfWork.SaveChangesAsync();
                return response;
            }
            catch
            {
                response.Succeeded = false;
                response.Errors.AddModelError("Start term", "Not found");
                return response;
            }
        }
    }
}