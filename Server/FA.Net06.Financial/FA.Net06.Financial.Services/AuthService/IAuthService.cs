﻿using FA.Net06.Financial.DTOs.Response;
using FA.Net06.Financial.DTOs.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Services.AuthService
{
    public interface IAuthService
    {
        Task<Response<LoginResultDTO>> Login(LoginUserDTO loginUser);
        IEnumerable<Claim> GetUserClaims(string tokenString);
    }
}
