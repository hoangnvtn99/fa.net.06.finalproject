﻿using FA.Net06.Financial.Data.Entities;
using FA.Net06.Financial.DTOs.Response;
using FA.Net06.Financial.DTOs.User;
using FA.Net06.Financial.Repositories.UserRepository;
using FA.Net06.Financial.Repository.Infrastructure;
using FA.Net06.Financial.Services.UserService;
using FA.Net06.Financial.Services.Validators;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Services.AuthService
{
    public class AuthService : IAuthService
    {
        private readonly IConfiguration _config;
        private readonly IUserService _userService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;

        public AuthService(IConfiguration config, IUserService userService, SignInManager<User> signInManager, UserManager<User> userManager, IUnitOfWork unitOfWork)
        {
            _config = config;
            _userService = userService;
            _signInManager = signInManager;
            _userManager = userManager;
            _unitOfWork = unitOfWork;
        }
        private string GetToken(IEnumerable<Claim> claims)
        {
            //header
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:SecurityKey"]));
            var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var header = new JwtHeader(signingCredentials);

            //payload
            var payload = new JwtPayload(_config["Jwt:Issuer"], _config["Jwt:Audience"], claims, DateTime.Now, DateTime.Now.AddMinutes(30));

            //jwt string
            var jst = new JwtSecurityToken(header, payload);

            return new JwtSecurityTokenHandler().WriteToken(jst);
        }

        public string GetRefreshToken()
        {
            var randomNumber = new byte[32];
            using (var randomNumberGenerator = RandomNumberGenerator.Create())
            {
                randomNumberGenerator.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }
        }

        public IEnumerable<Claim> GetUserClaims(string tokenString)
        {
            var token = new JwtSecurityTokenHandler().ReadJwtToken(tokenString);
            return token.Claims;
        }

        public async Task<Response<LoginResultDTO>> Login(LoginUserDTO loginUser)
        {
            var response = new Response<LoginResultDTO>();

            var validator = new LoginUserDTOValidator();
            var result = validator.Validate(loginUser);
            if (!result.IsValid)
            {
                response.Succeeded = false;
                result.AddToModelState(response.Errors);
                return response;
            }

            var user = await _userManager.FindByEmailAsync(loginUser.Email);
            if (user == null)
            {
                response.Succeeded = false;
                response.Errors.AddModelError("Login", "Username or password does not match");
                return response;
            }

            var loginResult = await _signInManager.PasswordSignInAsync(user, loginUser.Password, false, false);
            if (!loginResult.Succeeded)
            {
                response.Succeeded = false;
                response.Errors.AddModelError("Login", "Username or password does not match");
                return response;
            }

            response.Succeeded = true;
            var claims = await _userService.GetClaims(user.Id);
            var accessToken = GetToken(claims);
            var role = claims.First(claim => claim.Type == "Role").Value;
            var details = await _unitOfWork.UserRepository.GetDetails(user.Id);
            response.Data = new LoginResultDTO
            {
                AccessToken = accessToken,
                Role = role,
                UserName = user.UserName,
                Department = details.Department.Name
            };
            return response;
        }
    }
}

