﻿using FA.Net06.Financial.Data.Database;
using FA.Net06.Financial.Repositories.RoleRepository;
using FA.Net06.Financial.Repositories.FinancialPlanRepository;
using FA.Net06.Financial.Repositories.TermRepository;
using FA.Net06.Financial.Repositories.UserRepository;

namespace FA.Net06.Financial.Repository.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _db;
        public IUserRepository UserRepository { get { return new UserRepository(_db); } }
        public IRoleRepository RoleRepository { get { return new RoleRepository(_db); } }
        public ITermRepository TermRepository { get { return new TermRepository(_db); } }
        public IFinancialPlanRepository FinancialPlanRepository { get { return new FinancialPlanRepository(_db); } }

        public UnitOfWork(AppDbContext db)
        {
            _db = db;
        }

        public void Dispose()
        {
            _db.Dispose();
            GC.SuppressFinalize(this);
        }
        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _db.SaveChangesAsync();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
            {
                return;
            }
            _db?.Dispose();
        }
    }
}