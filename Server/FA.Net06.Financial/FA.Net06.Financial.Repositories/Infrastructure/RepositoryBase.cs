﻿
using FA.Net06.Financial.Data.Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Repository.Infrastructure
{
    public class RepositoryBase<TEntity> : IRepositoryBase<TEntity> where TEntity : class
    {
        protected readonly AppDbContext _db;
        protected DbSet<TEntity> _dbSet;

        public RepositoryBase(AppDbContext db)
        {
            _db = db;
            _dbSet = db.Set<TEntity>();
        }

        public async Task Create(TEntity entity)
        {
            await _dbSet.AddAsync(entity);
        }

        public async Task CreateRange(List<TEntity> entities)
        {
            await _dbSet.AddRangeAsync(entities);
        }

        public void Delete(TEntity entity)
        {
            _dbSet.Remove(entity);
        }

        public async Task Delete(Guid id)
        {
            _dbSet.Remove(await _dbSet.FindAsync(id));
        }

        public async Task<TEntity> Find(Guid id)
        {
            return await _dbSet.FindAsync(id);
        }

        public IEnumerable<TEntity> Find(List<Expression<Func<TEntity, bool>>> predicates)
        {
            var list = _dbSet.AsQueryable();
            foreach (var predicate in predicates)
                list = list.Where(predicate);
            return list;
        }

        public async Task<TEntity> FindByValue(Expression<Func<TEntity, bool>> predicate)
        {
            return await _dbSet.FirstOrDefaultAsync(predicate);
        }

        public async Task<IEnumerable<TEntity>> GetAll()
        {
            return await _dbSet.ToListAsync();
        }

        public void Update(TEntity entity)
        {
            _db.Update(entity);
        }

        public void UpdateRange(List<TEntity> entities)
        {
            _db.UpdateRange(entities);
        }

        public virtual IEnumerable<TEntity> GetPagedList(int page, int pageSize, List<Expression<Func<TEntity, bool>>> predicates, out int total)
        {
            var list = _dbSet.AsQueryable();
            foreach (var predicate in predicates)
                list = list.Where(predicate);
            total = list.Count();
            return list.Skip(pageSize * (page - 1)).Take(pageSize);
        }
    }
}