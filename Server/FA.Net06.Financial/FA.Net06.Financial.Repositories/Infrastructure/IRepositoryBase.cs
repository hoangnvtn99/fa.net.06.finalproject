﻿using System.Linq.Expressions;

namespace FA.Net06.Financial.Repository.Infrastructure
{
    public interface IRepositoryBase<TEntity> where TEntity : class
    {
        /// <summary>
        /// Change state of entity to added
        /// </summary>
        /// <param name="entity"></param>
        public Task Create(TEntity entity);

        /// <summary>
        ///  Change state of entities to added
        /// </summary>
        /// <param name="entities"></param>
        public Task CreateRange(List<TEntity> entities);

        /// <summary>
        /// Change state of entity to deleted
        /// </summary>
        /// <param name="entity"></param>
        public void Delete(TEntity entity);

        /// <summary>
        /// Delete <paramref name="TEntity"></paramref> from database
        /// </summary>
        /// <param name="ids"></param>
        /// <returns>void</returns>
        public Task Delete(Guid id);

        /// <summary>
        /// Change state of entity to modified
        /// </summary>
        /// <param name="entity"></param>
        public void Update(TEntity entity);

        /// <summary>
        /// Change state of entities to modified
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateRange(List<TEntity> entities);

        /// <summary>
        /// Get all <paramref name="TEntity"></paramref> from database by Id
        /// </summary>
        /// <returns>Return List TEntity</returns>
        public Task<IEnumerable<TEntity>> GetAll();

        /// <summary>
        /// Get <paramref name="TEntity"></paramref> from database
        /// </summary>
        /// <param name="ids"></param>
        /// <returns>Return TEntity if found else return other</returns>
        public Task<TEntity> Find(Guid primaryKey);

        /// <summary>
        /// Get List <paramref name="TEntity"></paramref> from database by criterias
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>Return List IEnumerable if found else null</returns>
        public IEnumerable<TEntity> Find(List<Expression<Func<TEntity, bool>>> predicates);

        /// <summary>
        ///    Get TEntity from database
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>Return TEntity if found else return null</returns>
        public Task<TEntity> FindByValue(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Get list with pagination and filter
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="predicates"></param>
        /// <param name="total"></param>
        /// <returns></returns>

        public IEnumerable<TEntity> GetPagedList(int page, int pageSize, List<Expression<Func<TEntity, bool>>> predicates, out int total);
    }
}