using FA.Net06.Financial.Repositories.RoleRepository;
using FA.Net06.Financial.Repositories.FinancialPlanRepository;
using FA.Net06.Financial.Repositories.TermRepository;
using FA.Net06.Financial.Repositories.UserRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Repository.Infrastructure
{
    public interface IUnitOfWork : IDisposable
    {
        public IUserRepository UserRepository { get; }
        public IRoleRepository RoleRepository { get; }
        public ITermRepository TermRepository { get; }
        public IFinancialPlanRepository FinancialPlanRepository { get; }
        int SaveChanges();
        Task<int> SaveChangesAsync();
    }
}