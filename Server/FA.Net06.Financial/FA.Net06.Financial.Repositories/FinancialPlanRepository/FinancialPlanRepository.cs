﻿using FA.Net06.Financial.Data.Database;
using FA.Net06.Financial.Data.Entities;
using FA.Net06.Financial.Repository.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Repositories.FinancialPlanRepository
{
    public class FinancialPlanRepository : RepositoryBase<FinancialPlan>, IFinancialPlanRepository
    {
        public FinancialPlanRepository(AppDbContext db) : base(db)
        {
        }

        public override IEnumerable<FinancialPlan> GetPagedList(int page, int pageSize, List<Expression<Func<FinancialPlan, bool>>> predicates, out int total)
        {
            var list = _dbSet.Include(plan => plan.Department).Include(plan => plan.Term).AsQueryable();
            foreach (var predicate in predicates)
                list = list.Where(predicate);
            total = list.Count();
            var statusList = new List<string> { "Denied", "New", "Waiting for Approval", "Approved", "Closed" };
            var orderedList = list.Select(plan => new { Plan = plan, StatusId = statusList.IndexOf(plan.Status) })
                                  .OrderBy(item => item.StatusId)
                                  .ThenBy(item => item.Plan.Term.StartDate)
                                  .Select(item => item.Plan);
            return orderedList.Skip(pageSize * (page - 1)).Take(pageSize);
        }
    }
}
