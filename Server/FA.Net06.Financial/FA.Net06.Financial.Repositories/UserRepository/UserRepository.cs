﻿using FA.Net06.Financial.Data.Database;
using FA.Net06.Financial.Data.Entities;
using FA.Net06.Financial.Repository.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Repositories.UserRepository
{
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        public UserRepository(AppDbContext db) : base(db)
        {

        }

        public async Task<User> GetDetails(Guid id)
        {
            var user = await _db.Users.Include(user => user.Roles).Include(user => user.Department).Include(user => user.Position).FirstAsync(user => user.Id == id);
            return user;
        }
        public async Task<Position> GetPosition(Guid id)
        {
            var position = _db.Positions.FirstOrDefault(position => position.Id == id);
            return position;
        }
        public async Task<Department> GetDepartment(Guid id)
        {
            var department = _db.Departments.FirstOrDefault(department => department.Id == id);
            return department;
        }

        public async Task<IEnumerable<User>> GetRole(string roleName)
        {
            var role = _db.Roles.First(role => role.Name == roleName);
            var user = _db.Users.Where(x => x.Roles == role).ToList();
            return user;
        }
    }
}
