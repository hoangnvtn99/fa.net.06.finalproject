﻿using FA.Net06.Financial.Data.Entities;
using FA.Net06.Financial.Repository.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Repositories.UserRepository
{
    public interface IUserRepository : IRepositoryBase<User>
    {
        Task<User> GetDetails(Guid id);
        Task<IEnumerable<User>> GetRole(string roleName);
        Task<Position> GetPosition(Guid id);
        Task<Department> GetDepartment(Guid id);
    }
}
