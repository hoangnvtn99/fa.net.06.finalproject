﻿using FA.Net06.Financial.Data.Database;
using FA.Net06.Financial.Data.Entities;
using FA.Net06.Financial.Repositories.UserRepository;
using FA.Net06.Financial.Repository.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Repositories.RoleRepository
{
    public class RoleRepository : RepositoryBase<Role>, IRoleRepository
    {
        public RoleRepository(AppDbContext db) : base(db)
        {

        }

    }
}
