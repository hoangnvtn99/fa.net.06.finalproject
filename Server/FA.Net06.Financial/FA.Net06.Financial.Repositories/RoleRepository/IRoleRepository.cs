﻿using FA.Net06.Financial.Data.Entities;
using FA.Net06.Financial.Repository.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Repositories.RoleRepository
{
    public interface IRoleRepository: IRepositoryBase<Role>
    {
    }
}
