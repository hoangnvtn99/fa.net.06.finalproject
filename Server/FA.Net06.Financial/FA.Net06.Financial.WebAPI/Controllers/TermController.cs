﻿using FA.Net06.Financial.DTOs.Term;
using FA.Net06.Financial.Services.TermService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FA.Net06.Financial.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TermController : ControllerBase
    {
        ITermService _termService;
        public TermController(ITermService termService)
        {
            _termService = termService;
        }

        [HttpPost]
        public async Task<IActionResult> Create(NewTermDTO newTerm)
        {
            var result = await _termService.Add(newTerm);
            if (result.Succeeded)
                return Ok();
            return BadRequest(result.Errors);
        }

        [HttpGet]
        public async Task<IActionResult> Get(string? termName, string? status, int pageIndex = 1, int pageSize = 10)
        {
            var response = _termService.GetPagedTerms(pageIndex, pageSize, status, termName);
            if (response.Succeeded)
                return Ok(response.Data);
            return BadRequest(response.Errors);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var response = await _termService.GetDetails(id);
            if (response.Succeeded)
                return Ok(response.Data);
            return NotFound(response.Errors);
        }

        [HttpPut]
        public async Task<IActionResult> Edit(EditTermDTO editTerm)
        {
            var response = await _termService.Edit(editTerm);
            if (response.Succeeded)
                return Ok();
            return NotFound(response.Errors);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var response = await _termService.Delete(id);
            if (response.Succeeded)
                return Ok();
            return NotFound(response.Errors);
        }

        [HttpPut("Start/{id}")]
        public async Task<IActionResult> Start(Guid id)
        {
            var response = await _termService.Start(id);
            if (response.Succeeded)
                return Ok();
            return BadRequest(response.Errors);
        }
    }
}
