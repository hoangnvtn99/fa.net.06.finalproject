﻿using FA.Net06.Financial.Services.AuthService;
using FA.Net06.Financial.Services.FinancialPlanService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FA.Net06.Financial.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FinancialPlanController : ControllerBase
    {
        private readonly IFinancialPlanService _financialPlanService;
        private readonly IAuthService _authService;
        public FinancialPlanController(IFinancialPlanService financialPlanService, IAuthService authService)
        {
            _financialPlanService = financialPlanService;
            _authService = authService;
        }

        [HttpGet]
        public IActionResult Get(string? name, string? department, string? term, string? status, int pageIndex = 1, int pageSize = 10)
        {
            var token = Request.Headers["Authorization"].ToString().Substring(7);
            var claims = _authService.GetUserClaims(token);
            var departmentOfViewer = claims.First(claim => claim.Type == "Department").Value;
            var roleOfViewer = claims.First(claim => claim.Type == "Role").Value;
            var response = _financialPlanService.GetPagedPlans(pageIndex, pageSize, name, department, term, status, departmentOfViewer, roleOfViewer);
            if (response.Succeeded)
                return Ok(response.Data);
            return NotFound(response.Errors);
        }

        [HttpGet("Import")]
        public IActionResult Import()
        {
            var response = _financialPlanService.Import();
            if (response.Succeeded)
                return Ok(response.Data);
            return BadRequest(response.Errors);
        }
    }
}