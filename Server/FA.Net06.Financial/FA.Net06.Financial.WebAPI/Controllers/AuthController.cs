﻿using FA.Net06.Financial.Data.Entities;
using FA.Net06.Financial.DTOs.Response;
using FA.Net06.Financial.DTOs.User;
using FA.Net06.Financial.Services.AuthService;
using FA.Net06.Financial.Services.UserService;
using FA.Net06.Financial.Services.Validators;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.IdentityModel.Tokens;
using System.Numerics;

namespace FA.Net06.Financial.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;

        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }

        [HttpPost("Login")]
        public async Task<IActionResult> Login(LoginUserDTO loginUser)
        {
            var loginResult = await _authService.Login(loginUser);
            if (loginResult.Succeeded)
                return Ok(loginResult.Data);
            return BadRequest(loginResult.Errors);
        }
    }
}
