﻿using FA.Net06.Financial.Data.Entities;
using FA.Net06.Financial.DTOs.Response;
using FA.Net06.Financial.DTOs.User;
using FA.Net06.Financial.Services.EmailService;
using FA.Net06.Financial.Services.UserService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Web;

namespace FA.Net06.Financial.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("ForgotPassword")]
        public async Task<IActionResult> ForgotPassword([FromBody] string email)
        {
            var response = await _userService.SendResetPasswordEmail(email);
            if (response.Succeeded)
                return Ok();
            return NotFound(response.Errors);
        }

        [HttpGet("ConfirmResetPassword/{token}")]
        public async Task<IActionResult> ConfirmResetPassword(string token)
        {
            var response = await _userService.ConfirmResetPassword(token);
            if (response.Succeeded)
                return Ok();
            return BadRequest(response.Errors);
        }

        [HttpPost("ResetPassword")]
        public async Task<IActionResult> ResetPassword(ResetPasswordDTO resetPasswordDTO)
        {
            resetPasswordDTO.Token = HttpUtility.UrlDecode(resetPasswordDTO.Token);
            var response = await _userService.ResetPassword(resetPasswordDTO);
            if (response.Succeeded)
                return Ok();
            return BadRequest(response.Errors);
        }

        [HttpPost("CreateUser")]
        public async Task<IActionResult> CreateUser(AddUserDTO newUser)
        {
            var response = await _userService.AddUser(newUser);
            if (response.Succeeded)
            {
                return Ok(response);
            }
            return BadRequest(response);
        }

        [HttpGet("DetailsUser/{id}")]
        public async Task<IActionResult> DetailsUser(Guid id)
        {
            var response = await _userService.DetailsUser(id);
            if (response.Succeeded)
            {
                return Ok(response);
            }
            return BadRequest(response);
        }

        [HttpGet("List-User")]
        public async Task<IActionResult> Get(string? userName, string?  roleName, int pageIndex = 1, int pageSize = 10)
        {
            var response = await _userService.GetPagedUsers(pageIndex, pageSize, roleName, userName);
            if (response.Succeeded)
                return Ok(response.Data);
            return BadRequest(response.Errors);
        }

        [HttpPut("UpdateUser")]
        public async Task<IActionResult> UpdateUser(UpdateUserDTO updateUser)
        {
            var response = await _userService.UpdateUser(updateUser);
            if (response.Succeeded)
                return Ok();
            return NotFound(response.Errors);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id)
        {
            var response = await _userService.Delete(id);
            if (response.Succeeded)
                return Ok();
            return NotFound(response.Errors);
        }
    }
}
