using FA.Net06.Financial.Data.Database;
using FA.Net06.Financial.Services;
using FA.Net06.Financial.Services.Mapper;
using Microsoft.EntityFrameworkCore;
using NLog;
using NLog.Web;
using FA.Net06.Financial.Repository.Infrastructure;
using FA.Net06.Financial.Services.UserService;
using FA.Net06.Financial.Services.AuthService;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.AspNetCore.Identity;
using FA.Net06.Financial.Data.Entities;
using FA.Net06.Financial.Services.EmailService;
using FluentValidation.AspNetCore;
using FA.Net06.Financial.Services.FinancialPlanService;
using FA.Net06.Financial.Services.TermService;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.OpenApi.Models;

var logger = NLog.LogManager.Setup().LoadConfigurationFromAppSettings().GetCurrentClassLogger();
logger.Debug("init main");

try
{
    var builder = WebApplication.CreateBuilder(args);

    // Add services to the container.
    builder.Services.AddControllers();

    // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
    builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSwaggerGen(options =>
    {
        var securityScheme = new OpenApiSecurityScheme
        {
            Name = "JWT Authentication",
            Description = "Enter a valid JWT bearer token",
            In = ParameterLocation.Header,
            Type = SecuritySchemeType.Http,
            Scheme = "bearer",
            BearerFormat = "JWT",
            Reference = new OpenApiReference
            {
                Id = JwtBearerDefaults.AuthenticationScheme,
                Type = ReferenceType.SecurityScheme
            }
        };

        options.AddSecurityDefinition(securityScheme.Reference.Id, securityScheme);
        options.AddSecurityRequirement(new OpenApiSecurityRequirement
        {
            {securityScheme, new string[]{} }
        });
    });

    builder.Services.AddFluentValidation(options => options.RegisterValidatorsFromAssemblyContaining<Program>());
    // Config Dabase
    builder.Services.AddDbContext<AppDbContext>(options =>
    {
        options.UseSqlServer(builder.Configuration.GetConnectionString("MSSQL"));
    });

    builder.Services.AddIdentity<User, Role>().AddEntityFrameworkStores<AppDbContext>().AddDefaultTokenProviders();

    builder.Services.AddAutoMapper(typeof(AutoMapperConfig));
    //Config Cors
    builder.Services.AddCors(options =>
    {
        options.AddPolicy("Cors", builder =>
        {
            builder.WithOrigins("*").AllowAnyMethod().AllowAnyHeader();
        });
    });

    //Add Service
    builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();

    builder.Services.AddScoped<IUserService, UserService>();

    builder.Services.AddScoped<IAuthService, AuthService>();

    builder.Services.AddScoped<IEmailService, EmailService>();

    builder.Services.AddScoped<ITermService, TermService>();

    builder.Services.AddScoped<IFinancialPlanService, FinancialPlanService>();

    builder.Services.AddAuthentication("Bearer").AddJwtBearer("Bearer", options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters()
        {
            ValidateIssuer = true,
            ValidateAudience = true,
            ValidateIssuerSigningKey = true,
            ValidateLifetime = true,
            ValidIssuer = builder.Configuration["Jwt:Issuer"],
            ValidAudience = builder.Configuration["Jwt:Audience"],
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration["Jwt:SecurityKey"])),
        };
    });

    builder.Services.AddAuthorization(config =>
    {
        config.AddPolicy("OnlyStaff", policyConfig =>
        {
            policyConfig.RequireClaim("Role", "Staff");
        });
        config.AddPolicy("OnlyAccountant", policyConfig =>
        {
            policyConfig.RequireClaim("Role", "Accountant");
        });
        config.AddPolicy("OnlyAdmin", policyConfig =>
        {
            policyConfig.RequireClaim("Role", "Admin");
        });
    });

    var app = builder.Build();

    app.UseStaticFiles();
    //Configure the HTTP request pipeline.
    app.UseSwagger();
    app.UseSwaggerUI();

    app.UseCors("Cors");

    app.UseHttpsRedirection();

    app.UseAuthorization();

    app.MapControllers();

    app.Run();
}
catch (Exception exception)
{
    // NLog: catch setup errors
    logger.Error(exception, "Stopped program because of exception");
}
finally
{
    // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
    NLog.LogManager.Shutdown();
}