﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.DTOs.User
{
    public class UserDetailsDTO
    {
        // Id
        public Guid Id { get; set; }

        // Username
        public string UserName { get; set; }

        // Full Name 
        public string FullName { get; set; }

        // Email
        public string Email { get; set; }

        // Phone Number 
        public string PhoneNumber { get; set; }

        // D.O.B
        public DateTime DateTime { get; set; }

        // Department 
        public string DepartmentName { get; set; }

        // Position 
        public string PositionName { get; set; }

        // Role 
        public string RoleName { get; set; }

        // Status 
        public string Status { get; set; }

        // Note 
        public string Note { get; set; }
    }
}
