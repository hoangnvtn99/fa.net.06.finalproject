﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.DTOs.User
{
    public class UpdateUserDTO
    {
        // ID
        public Guid Id { get; set; } 
        // Full Name 
        public string FullName { get; set; }

        // Email
        public string Email { get; set; }

        // Phone Number 
        public string PhoneNumber { get; set; }

        // D.O.B
        public DateTime DOB { get; set; }

        // Address
        public string Address { get; set; }

        // Department 
        public Guid DepartmentId { get; set; }

        // Position 
        public Guid PositionId { get; set; }

        // Role
        public Guid RoleId { get; set; }

        // Note 
        public string Note { get; set; }
    }
}
