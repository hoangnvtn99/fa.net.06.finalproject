﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.DTOs.User
{
    public class AddUserDTO
    {
        // Email
        public string Email { get; set; } = string.Empty;

        // Date of Birth 
        public DateTime DOB { get; set; }

        // Department 
        public Guid DepartmentId { get; set; }

        // Role 
        public Guid RoleId { get; set; }

        // Full name
        public string Fullname { get; set; } = string.Empty;

        // Phone Number
        public string Phonenumber { get; set; } = string.Empty;

        // Address
        public string Address { get; set; } = string.Empty;

        // Position 
        public Guid PositionId { get; set; }

        // Note 
        public string Note { get; set; }
    }
}
