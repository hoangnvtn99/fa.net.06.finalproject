﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.DTOs.User
{
    public class LoginResultDTO
    {
        public string AccessToken { get; set; } = null!;
        public string Role { get; set; } = null!;
        public string UserName { get; set; } = null!;
        public string Department { get; set; } = null!;
    }
}
