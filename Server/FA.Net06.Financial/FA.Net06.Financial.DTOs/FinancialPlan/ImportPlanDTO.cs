﻿using FA.Net06.Financial.DTOs.Term;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.DTOs.FinancialPlan
{
    public class ImportPlanDTO
    {
        public IEnumerable<TermDTO> Terms { get; set; }
        public string FileTemplateUrl { get; set; } = null!;
    }
}
