﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.DTOs.FinancialPlan
{
    public class FinancialPlanDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Term { get; set; }
        public string Department { get; set; }
        public string Status { get; set; }
        public string Uploader { get; set; }
        public int Version { get; set; }

    }
}
