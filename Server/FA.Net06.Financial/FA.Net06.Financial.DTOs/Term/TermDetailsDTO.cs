﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.DTOs.Term
{
    public class TermDetailsDTO
    {
        public string Name { get; set; }
        public string Duration { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime PlanDueDate { get; set; }
        public DateTime ReportDueDate { get; set; }
        public string Status { get; set; }
    }
}
