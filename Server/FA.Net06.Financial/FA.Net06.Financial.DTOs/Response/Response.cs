﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace FA.Net06.Financial.DTOs.Response
{
    public class Response<T> where T : class
    {
        public ModelStateDictionary Errors { get; set; } = new ModelStateDictionary();
        public T? Data { get; set; }
        public bool Succeeded { get; set; }

    }
}