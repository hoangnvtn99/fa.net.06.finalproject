﻿using FA.Net06.Financial.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Data.Config
{
    public class FinancialPlanConfig : IEntityTypeConfiguration<FinancialPlan>
    {
        public void Configure(EntityTypeBuilder<FinancialPlan> builder)
        {
            builder.HasCheckConstraint("CK_Status_Values", "Status IN ('New', 'Waiting for Approval', 'Approved', 'Denied', 'Closed')")
                   .Property(plan => plan.Status).HasColumnType("varchar(20)");
            builder.HasCheckConstraint("CK_Version_MinValue", "Version > 0");
            builder.HasOne(plan => plan.User).WithMany(user => user.Plans).HasForeignKey(plan => plan.UserId).OnDelete(DeleteBehavior.NoAction);
        }
    }
}