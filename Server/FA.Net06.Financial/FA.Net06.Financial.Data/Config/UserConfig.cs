﻿using FA.Net06.Financial.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Data.Config
{
    public class UserConfig : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasMany(user => user.Roles).WithMany(role => role.Users).UsingEntity<IdentityUserRole<Guid>>();
            builder.Property(user => user.FullName).HasColumnType("nvarchar(50)");
            builder.Property(user => user.DOB).HasColumnType("date");
            builder.Property(user => user.Address).HasColumnType("nvarchar(200)");
            builder.HasCheckConstraint("CK_Status_Values", "Status IN ('Active', 'Inactive')")
                   .Property(user => user.Status).HasColumnType("varchar(8)");
            builder.Property(user => user.Note).HasColumnType("text");

        }
    }
}
