﻿using FA.Net06.Financial.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Data.Config
{
    public class SuppilerConfig : IEntityTypeConfiguration<Suppiler>
    {
        public void Configure(EntityTypeBuilder<Suppiler> builder)
        {
            builder.Property(suppiler => suppiler.Name).HasColumnType("nvarchar(50)");
        }
    }
}
