﻿using FA.Net06.Financial.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Data.Config
{
    public class TermConfig : IEntityTypeConfiguration<Term>
    {
        public void Configure(EntityTypeBuilder<Term> builder)
        {
            builder.Property(term => term.Name).HasColumnType("nvarchar(100)");
            builder.HasCheckConstraint("CK_Duration_Values", "Duration IN ('Monthly', 'Quarterly', 'Half-year')")
                   .Property(term => term.Duration).HasColumnType("varchar(9)");
            builder.Property(term => term.StartDate).HasColumnType("date");
            builder.Property(term => term.EndDate).HasColumnType("date");
            builder.Property(term => term.PlanDueDate).HasColumnType("date");
            builder.Property(term => term.ReportDueDate).HasColumnType("date");
            builder.HasCheckConstraint("CK_Status_Values", "Status IN ('New', 'In-Progress', 'Closed')")
                   .Property(term => term.Status).HasColumnType("char(10)");
        }
    }
}
