﻿using FA.Net06.Financial.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Data.Config
{
    public class CostTypeConfig : IEntityTypeConfiguration<CostType>
    {
        public void Configure(EntityTypeBuilder<CostType> builder)
        {
            builder.Property(costType => costType.Name).HasColumnType("nvarchar(50)");
        }
    }
}
