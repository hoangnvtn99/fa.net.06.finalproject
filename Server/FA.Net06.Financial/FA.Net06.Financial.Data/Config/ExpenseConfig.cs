﻿using FA.Net06.Financial.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Data.Config
{
    public class ExpenseConfig : IEntityTypeConfiguration<Expense>
    {
        public void Configure(EntityTypeBuilder<Expense> builder)
        {
            builder.HasKey(expense => new { expense.Id, expense.PlanId });
            builder.Property(expense => expense.Name).HasColumnType("nvarchar(50)");
            builder.Property(expense => expense.UnitPrice).HasColumnType("money");
            builder.Property(expense => expense.Notes).HasColumnType("ntext");
            builder.Property(expense => expense.ExpenseStatus).HasColumnType("nvarchar(10)");
            builder.HasCheckConstraint("CK_Amount_MinValue", "Amount > 0");
            builder.HasCheckConstraint("CK_ExpenseStatus_Values", "ExpenseStatus IN ('New', 'Waiting for Approval', 'Approved', 'Closed')");
        }
    }
}
