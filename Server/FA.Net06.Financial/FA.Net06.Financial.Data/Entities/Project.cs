﻿using FA.Net06.Financial.Data.Entities.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Data.Entities
{
    public class Project : BaseEntity<Guid>
    {
        public string Name { get; set; } = null!;
        public IEnumerable<Expense> Expenses { get; set; }
    }
}
