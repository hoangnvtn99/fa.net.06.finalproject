﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Data.Entities
{
    public class User : IdentityUser<Guid>
    {
        public string FullName { get; set; } = null!;
        public DateTime DOB { get; set; }
        public string? Address { get; set; }
        public string Status { get; set; } = null!;
        public string? Note { get; set; }
        public string? ResetPasswordToken { get; set; }
        public DateTime? ResetPasswordExpire { get; set; }
        public Guid PositionId { get; set; }
        public Guid DepartmentId { get; set; }

        public Position Position { get; set; }
        public Department Department { get; set; }
        public IEnumerable<Role> Roles { get; set; }
        public IEnumerable<FinancialPlan> Plans { get; set; }
    }
}
