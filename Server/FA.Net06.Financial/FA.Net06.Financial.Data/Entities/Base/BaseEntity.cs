﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Data.Entities.Base
{
    public class BaseEntity<T>
    {
        public T Id { get; set; }
    }
}
