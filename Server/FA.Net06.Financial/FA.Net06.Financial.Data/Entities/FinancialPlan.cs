﻿using FA.Net06.Financial.Data.Entities;
using FA.Net06.Financial.Data.Entities.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Data.Entities
{
    public class FinancialPlan : BaseEntity<Guid>
    {
        public string Name { get; set; }
        public string Status { get; set; } = null!;
        public int Version { get; set; }
        public Guid TermId { get; set; }
        public Guid DepartmentId { get; set; }
        public Guid UserId { get; set; }

        public Term Term { get; set; }
        public Department Department { get; set; }
        public User User { get; set; }
    }
}
