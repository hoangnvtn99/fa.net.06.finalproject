﻿using FA.Net06.Financial.Data.Entities.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Data.Entities
{
    public class Term : BaseEntity<Guid>
    {
        public string Name { get; set; } = null!;
        public string Duration { get; set; } = null!;
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime PlanDueDate { get; set; }
        public DateTime ReportDueDate { get; set; }
        public string Status { get; set; } = null!;

        public IEnumerable<FinancialPlan> FinancialPlans { get; set; }
    }
}
