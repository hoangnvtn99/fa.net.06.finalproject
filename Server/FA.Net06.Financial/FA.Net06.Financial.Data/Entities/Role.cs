﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Data.Entities
{
    public class Role : IdentityRole<Guid>
    {
        public IEnumerable<User> Users { get; set; }
    }
}
