﻿using FA.Net06.Financial.Data.Entities.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Data.Entities
{
    public class Expense : BaseEntity<Guid>
    {
        public Guid PlanId { get; set; }
        public string Name { get; set; }
        public decimal UnitPrice { get; set; }
        public int Amount { get; set; }
        public string PIC { get; set; } = null!;
        public string Notes { get; set; }
        public string ExpenseStatus { get; set; }
        public Guid CostTypeId { get; set; }
        public Guid ProjectId { get; set; }
        public Guid SuppilerId { get; set; }

        public CostType CostType { get; set; }
        public Project Project { get; set; }
        public Suppiler Suppiler { get; set; }
        public FinancialPlan Plan { get; set; }
    }
}
