﻿using FA.Net06.Financial.Data.Entities.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Net06.Financial.Data.Entities
{
    public class Department : BaseEntity<Guid>
    {
        public string Name { get; set; } = null!;
        public string Code { get; set; } = null!;

        public IEnumerable<FinancialPlan> FinancialPlans { get; set; }
        public IEnumerable<User> Users { get; set; }
    }
}
