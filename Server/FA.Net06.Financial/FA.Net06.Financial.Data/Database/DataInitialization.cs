﻿using FA.Net06.Financial.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace FA.Net06.Financial.Data.Database
{
    public static class DataInitialization
    {
        public static void Seed(this ModelBuilder builder)
        {
            var rd = new Random();
            var departments = GenerateDepartments();
            builder.Entity<Department>().HasData(departments);

            var positions = GeneratePositions();
            builder.Entity<Position>().HasData(positions);

            var roles = GenerateRoles();
            builder.Entity<Role>().HasData(roles);

            var hasher = new PasswordHasher<User>();
            var users = new User[]
            {
                new User
                {
                    Id = Guid.NewGuid(),
                    FullName = "Nguyễn Quốc Tuấn",
                    Status = "Active",
                    UserName = "nqtuan204",
                    NormalizedUserName = "NQTUAN204",
                    Email = "nqtuan204@gmail.com",
                    NormalizedEmail = "NQTUAN204@GMAIL.COM",
                    EmailConfirmed = true,
                    AccessFailedCount = 0,
                    DOB = new DateTime(1996,4,20),
                    PasswordHash = hasher.HashPassword(null,"Matkhau1234!"),
                    SecurityStamp = Guid.NewGuid().ToString(),
                    DepartmentId = departments[rd.Next(departments.Length)].Id,
                    PositionId = positions[rd.Next(positions.Length)].Id
                },
                new User
                {
                    Id = Guid.NewGuid(),
                    FullName = "Nguyễn Anh Quân",
                    Status = "Active",
                    UserName = "nguyenanhquan2801",
                    NormalizedUserName = "NGUYENANHQUAN2801",
                    Email = "nguyenanhquan2801@gmail.com",
                    NormalizedEmail = "NGUYENANHQUAN2801@GMAIL.COM",
                    EmailConfirmed = true,
                    AccessFailedCount = 0,
                    DOB = new DateTime(1996,4,20),
                    PasswordHash = hasher.HashPassword(null,"Matkhau1234!"),
                    SecurityStamp = Guid.NewGuid().ToString(),
                    DepartmentId = departments[rd.Next(departments.Length)].Id,
                    PositionId = positions[rd.Next(positions.Length)].Id
                },
                new User
                {
                    Id = Guid.NewGuid(),
                    FullName = "Nguyễn Thế Tân",
                    Status = "Active",
                    UserName = "tanhn22051999",
                    NormalizedUserName = "TANHN22051999",
                    Email = "tanhn22051999@gmail.com",
                    NormalizedEmail = "TANHN22051999@GMAIL.COM",
                    EmailConfirmed = true,
                    AccessFailedCount = 0,
                    DOB = new DateTime(1996,4,20),
                    PasswordHash = hasher.HashPassword(null,"Matkhau1234!"),
                    SecurityStamp = Guid.NewGuid().ToString(),
                    DepartmentId = departments[rd.Next(departments.Length)].Id,
                    PositionId = positions[rd.Next(positions.Length)].Id
                }
            };
            builder.Entity<User>().HasData(users);

            var userRoles = GenerateUserRoles(users, roles);
            builder.Entity<IdentityUserRole<Guid>>().HasData(userRoles);
        }

        private static Department[] GenerateDepartments()
        {
            int size = 10;
            var departments = new Department[size];
            for (int i = 1; i <= size; i++)
            {
                departments[i - 1] = new Department
                {
                    Id = Guid.NewGuid(),
                    Name = $"Business Unit {i}",
                    Code = $"BU{i}"
                };
            }
            return departments;
        }

        private static Position[] GeneratePositions()
        {
            return new Position[]
            {
                new Position
                {
                    Id = Guid.NewGuid(),
                    Name = "Tech lead"
                },
                new Position
                {
                    Id = Guid.NewGuid(),
                    Name = "Accountant"
                },
                new Position
                {
                    Id = Guid.NewGuid(),
                    Name = "Marketing expert"
                },
                new Position
                {
                    Id = Guid.NewGuid(),
                    Name = "Financial excutive"
                }
            };
        }

        private static Role[] GenerateRoles()
        {
            return new Role[]
            {
                new Role
                {
                    Id = Guid.NewGuid(),
                    Name = "Admin"
                },
                new Role
                {
                    Id = Guid.NewGuid(),
                    Name = "Staff"
                },
                new Role
                {
                    Id = Guid.NewGuid(),
                    Name = "Accountant"
                }
            };
        }

        private static IdentityUserRole<Guid>[] GenerateUserRoles(User[] users, Role[] roles)
        {
            var rd = new Random();
            var userRoles = new IdentityUserRole<Guid>[users.Length];
            for (int i = 0; i < userRoles.Length; i++)
                userRoles[i] = new IdentityUserRole<Guid> { UserId = users[i].Id, RoleId = roles[rd.Next(roles.Length)].Id };
            return userRoles;
        }
    }
}