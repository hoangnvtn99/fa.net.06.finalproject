﻿using FA.Net06.Financial.Data.Entities;
using FA.Net06.Financial.Data.Config;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Data;

namespace FA.Net06.Financial.Data.Database
{
    public class AppDbContext : IdentityDbContext<User, Role, Guid>
    {
        public DbSet<CostType> CostTypes { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Suppiler> Suppilers { get; set; }
        public DbSet<Term> Terms { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<FinancialPlan> FinancialPlans { get; set; }
        public DbSet<Expense> Expenses { get; set; }

        public AppDbContext() { }
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
            //Database.Migrate();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            foreach (var entityType in builder.Model.GetEntityTypes())
            {
                var tableName = entityType.GetTableName();
                if (tableName!.StartsWith("AspNet"))
                    entityType.SetTableName(tableName.Substring(6));
            }

            builder.ApplyConfiguration(new CostTypeConfig());
            builder.ApplyConfiguration(new DepartmentConfig());
            builder.ApplyConfiguration(new FinancialPlanConfig());
            builder.ApplyConfiguration(new PositionConfig());
            builder.ApplyConfiguration(new ProjectConfig());
            builder.ApplyConfiguration(new SuppilerConfig());
            builder.ApplyConfiguration(new TermConfig());
            builder.ApplyConfiguration(new UserConfig());
            builder.ApplyConfiguration(new ExpenseConfig());
            builder.Seed();
        }
    }
}