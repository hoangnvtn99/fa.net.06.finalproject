﻿using FA.Net06.Financial.Data.Database;
using FA.Net06.Financial.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace FA.Net06.Financial.UnitTest.FA.Net06.Finacial.Data.Test
{
    public class SetUpFakeData
    {
        public AppDbContext GetFakeDbContext()
        {
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseInMemoryDatabase(databaseName: "FA.Net06.Financial")
                .Options;
            var context = new AppDbContext(options);
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();
            return context;
        }

        private List<Department> GenerateDepartments()
        {
            int size = 10;
            var departments = new List<Department>();
            for (int i = 1; i <= size; i++)
            {
                departments.Add(new Department
                {
                    Id = Guid.NewGuid(),
                    Name = $"Business Unit {i}",
                    Code = $"BU{i}"
                });
            }

            return departments;
        }

        private List<Position> GeneratePositions()
        {
            return new List<Position>
            {
                new Position
                {
                    Id = Guid.NewGuid(),
                    Name = "Tech lead"
                },
                new Position
                {
                    Id = Guid.NewGuid(),
                    Name = "Accountant"
                },
                new Position
                {
                    Id = Guid.NewGuid(),
                    Name = "Marketing expert"
                },
                new Position
                {
                    Id = Guid.NewGuid(),
                    Name = "Financial excutive"
                }
            };
        }

        private List<Role> GenerateRoles()
        {
            return new List<Role>
            {
                new Role
                {
                    Id = Guid.NewGuid(),
                    Name = "Admin",
                    NormalizedName = "ADMIN"
                },
                new Role
                {
                    Id = Guid.NewGuid(),
                    Name = "Manager",
                    NormalizedName = "MANAGER"
                },
            };
        }

        private List<User> GenerateUsers(List<Department> departments, List<Position> positions)
        {
            var rd = new Random();
            var users = new List<User>();
            for (int i = 1; i <= 10; i++)
            {
                users.Add(new User
                {
                    Id = Guid.NewGuid(),
                    UserName = $"user{i}",
                    NormalizedUserName = $"USER{i}",
                    Email = $"email{i}",
                    NormalizedEmail = $"EMAIL{i}",
                    EmailConfirmed = true,
                    DOB = DateTime.Now.AddYears(-20),
                    DepartmentId = departments[rd.Next(departments.Count)].Id,
                    PositionId = positions[rd.Next(positions.Count)].Id,
                    PasswordHash = new PasswordHasher<User>().HashPassword(null! ,"Matkhau1234!"),
                    SecurityStamp = Guid.NewGuid().ToString(),
                });
            }

            return users;
        }

        private List<IdentityUserRole<Guid>> GenerateUserRoles(List<User> users, List<Role> roles)
        {
            var userRoles = new List<IdentityUserRole<Guid>>();
            foreach (var user in users)
            {
                userRoles.Add(new IdentityUserRole<Guid>
                {
                    RoleId = roles[0].Id,
                    UserId = user.Id
                });
            }

            return userRoles;
        }

        public void SeedData(AppDbContext context)
        {
            var departments = GenerateDepartments();
            var positions = GeneratePositions();
            var roles = GenerateRoles();
            var users = GenerateUsers(departments, positions);
            var userRoles = GenerateUserRoles(users, roles);
            context.AddRange(departments);
            context.AddRange(positions);
            context.AddRange(roles);
            context.AddRange(users);
            context.AddRange(userRoles);
            context.SaveChanges();
            context.Database.EnsureCreated();
        }

        public void ClearData(AppDbContext context)
        {
            context.RemoveRange(context.UserRoles);
            context.RemoveRange(context.Roles);
            context.RemoveRange(context.Users);
            context.RemoveRange(context.Departments);
            context.RemoveRange(context.Positions);
            context.SaveChanges();
            context.Database.EnsureDeleted();
        }
    }
}